﻿
        $(document).ready(function () {
            ko.applyBindings(new UsersViewModel());
        });

     
        function UsersViewModel() {

            var self = this;      
            self.users = ko.observableArray(); 
          
            self.remove = function () {
               
                $.ajax({
                    type: "DELETE",
                    url: "/api/users"
                }).done(function () {
                    self.users.removeAll();
                });
            }
          
            $.getJSON("/api/users", self.users);
        }