﻿using InstaRetailCodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace InstaRetailCodeChallenge.Controllers
{
    public class UploadUser
    {
        public void UploadCustomerData(string filename)
        {
            DataTable dtCustomers = ProcessCSV(filename);
            foreach (DataRow row in dtCustomers.Rows)
            {
                User customer = new User();
                customer.FirstName = row[0].ToString();
                customer.LastName = row[1].ToString();
                customer.Address = row[2].ToString();

                using (var ctx = new UserContext())
                {
                    ctx.Users.Add(customer);
                    ctx.SaveChanges();
                }
            }
        }

        private static DataTable ProcessCSV(string fileName)
        {
            //Set up our variables
            //string Feedback = string.Empty;
            string line = string.Empty;
            string[] strArray;
            DataTable dt = new DataTable();
            DataRow row;
            // work out where we should split on comma, but not in a sentence
            Regex r = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");
            //Set the filename in to our stream
            StreamReader sr = new StreamReader(fileName);
            string[] headers = sr.ReadLine().Split(',');
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            //Read the first line and split the string at , with our regular expression in to an array
            //line = sr.ReadLine();
            strArray = r.Split(line);         

            //Read each line in the CVS file until it’s empty
            while ((line = sr.ReadLine()) != null)
            {
                row = dt.NewRow();

                //add our current value to our data row
                row.ItemArray = r.Split(line);
                dt.Rows.Add(row);
            }

            //Tidy Streameader up
            sr.Dispose();

            //return a the new DataTable
            return dt;

        }
    }
}