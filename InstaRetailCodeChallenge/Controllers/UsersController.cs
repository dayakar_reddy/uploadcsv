﻿using InstaRetailCodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace InstaRetailCodeChallenge.Controllers
{
    public class UsersController : ApiController
    {
        UserRepository cr = new UserRepository();

        [HttpGet]
        public IEnumerable<User> GetAllCustomers()
        {
            return cr.GetAllCustomers();
        }

        [HttpDelete]
        public void Delete()
        {
            cr.Delete();
        }
    }
}
