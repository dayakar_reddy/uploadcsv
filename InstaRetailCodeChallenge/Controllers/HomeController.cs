﻿using InstaRetailCodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace InstaRetailCodeChallenge.Controllers
{
    public class HomeController : Controller
    {
        UploadUser uploadCus = new UploadUser();
        public ActionResult Index(HttpPostedFileBase file)
        {
            DataTable dtCustomers = new DataTable();
            
            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
                else if (file.ContentLength > 0)
                {


                    //TO:DO
                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/Upload"), fileName);
                    file.SaveAs(path);
                    uploadCus.UploadCustomerData(path);              
                  

                    ModelState.Clear();
                    ViewBag.Message = "File uploaded successfully";

                }
            }
            return View();
        }
       
    }
}
