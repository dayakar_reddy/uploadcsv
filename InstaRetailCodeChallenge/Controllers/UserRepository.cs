﻿using InstaRetailCodeChallenge.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InstaRetailCodeChallenge.Controllers
{
    public class UserRepository
    {
        UserContext db = new UserContext();
        
        public IEnumerable<User> GetAllCustomers()
        {
            return db.Users.ToList();
        }

        
        public void Delete()
        {
            var userToClear = from c in db.Users
                              select c;
            foreach (User cr in userToClear)
            {
                db.Users.Remove(cr);

            }
            db.SaveChanges();
        }
    }
}